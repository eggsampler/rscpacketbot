package main

import (
	"fmt"
	"io"
	"strconv"
)

type BitReader struct {
	Bits   string
	Offset int
}

func NewBitReader(data []byte) BitReader {
	br := BitReader{}
	br.Bits = fmt.Sprintf("%08b", data[0])
	for i := 1; i < len(data); i++ {
		br.Bits += fmt.Sprintf("%08b", data[i])
	}
	return br
}

func (br *BitReader) Read(size int) (int, error) {
	end := br.Offset + size
	if end > len(br.Bits) {
		return 0, io.EOF
	}
	i, err := strconv.ParseInt(br.Bits[br.Offset:end], 2, 32)
	if err != nil {
		return 0, err
	}
	br.Offset += size
	return int(i), nil
}
