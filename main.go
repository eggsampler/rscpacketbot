package main

import (
	"bufio"
	"bytes"
	"encoding/hex"
	"fmt"
	"math/rand"
	"net"

	"encoding/binary"

	"io"

	"bitbucket.org/eggsampler/rscpacketbot/binstruct"
	"bitbucket.org/eggsampler/rscpacketbot/log"
	"bitbucket.org/eggsampler/rscpacketbot/opcode"
	"bitbucket.org/eggsampler/rscpacketbot/rsccrypto"
)

const (
	VERSION = 204

	MAX_WAYPOINTS = 10
	MAX_STATS     = 18
	MAX_FRIENDS   = 200
	MAX_IGNORE    = 200
	MAX_INVENTORY = 35
	MAX_OBJECTS   = 1500

	MAGIC_LOC = 128
)

type (
	Character struct {
		Index         uint16
		AnimationNext int
		X, Y          int
		Waypoint      struct {
			Current int
			X       [MAX_WAYPOINTS]int
			Y       [MAX_WAYPOINTS]int
		}
	}

	CharacterList struct {
		Count int
		List  map[int]*Character
	}

	Object struct {
		Id        uint16
		X         int
		Y         int
		Direction uint8
	}
)

var (
	LocalPlayer Character

	Players CharacterList

	NPC CharacterList

	Objects struct {
		Count int
		List  [MAX_OBJECTS]Object
	}

	WallObjects struct {
		Count int
		List  [MAX_OBJECTS]Object
	}

	Info struct {
		LastIp         uint32
		LastLoggedIn   uint16
		LastRecovery   byte
		UnreadMessages uint16
	}

	World struct {
		PlayerIndex     uint16
		PlaneWidth      uint16
		PlaneHeight     uint16
		PlayerHeight    uint16
		PlaneMultiplier uint16
	}

	Stats struct {
		Current     [MAX_STATS]uint8
		Base        [MAX_STATS]uint8
		Experience  [MAX_STATS]uint32
		QuestPoints uint8
	}

	EquipmentStats struct {
		Armour      uint16
		WeaponAim   uint16
		WeaponPower uint16
		Magic       uint16
		Prayer      uint16
		Ranged      uint16
	}

	Inventory struct {
		Count      uint8
		Id         [MAX_INVENTORY]uint16
		Equipped   [MAX_INVENTORY]uint16
		StackCount [MAX_INVENTORY]uint32
	}

	Settings struct {
		Privacy [4]uint8
		Game    [3]uint8
	}

	Friends struct {
		Count  uint8
		Hash   [MAX_FRIENDS]uint32
		Online [MAX_FRIENDS]uint8
	}

	Ignore struct {
		Count uint8
		Hash  [MAX_IGNORE]uint32 `struct_sizefield:"Count"`
	}
)

func main() {
	if err := loadData(); err != nil {
		log.WithError(err).Fatal("loading data")
	}

	conn, err := net.Dial("tcp", "127.0.0.1:27337") // firescape.online:27337")
	if err != nil {
		log.WithError(err).Fatal("connecting")
	}
	log.WithField("addr", conn.RemoteAddr()).Info("connected")

	reader := bufio.NewReader(conn)

	if err := login(reader, conn, "eggsampler", "hello123"); err != nil {
		log.WithError(err).Fatal("logging in")
	}

	// channel which receives errors from created goroutines
	// eg, packet reader goroutine which would error when the network connection is closed
	errChan := make(chan error)

	// channel which receives read packets
	packetReader := newPacketReader(errChan, reader)

	for {

		select {
		case err := <-errChan:
			log.WithError(err).Fatal("error")

		case packet := <-packetReader:
			handlePacket(packet[0], packet[1:])
		}

	}
}

func login(reader *bufio.Reader, conn net.Conn, username, password string) error {

	// write session/helo
	heloPacket := struct {
		UserLoginHash uint8
		ClassName     string
	}{
		UserLoginHash: uint8(int((usernameLoginHash(username) >> 16) & 31)),
		ClassName:     "dickbutt",
	}
	if err := writePacket(conn, opcode.Id(VERSION, opcode.CL_SESSION), heloPacket); err != nil {
		return fmt.Errorf("writing helo packet: %v", err)
	}

	// read session id
	var sessionId int64
	if err := binstruct.Read(reader, binary.BigEndian, &sessionId); err != nil {
		return fmt.Errorf("reading session id: %v", err)
	}

	// encrypted auth block
	encBlock := struct {
		Seed     [4]int32
		LinkUid  int32
		Username string `struct_delim:"\x0A"`
		Password string `struct_delim:"\x0A"`
	}{
		Seed: [4]int32{
			rand.Int31(),
			rand.Int31(),
			int32(sessionId >> 32),
			int32(sessionId),
		},
		LinkUid:  0,
		Username: formatAuthString(username, 20),
		Password: formatAuthString(password, 20),
	}
	var encBlockBuffer bytes.Buffer
	if err := binstruct.Write(&encBlockBuffer, binary.BigEndian, encBlock); err != nil {
		return fmt.Errorf("writing encrypted auth block: %v", err)
	}
	encBlockData := rsccrypto.RsaEncrypt(encBlockBuffer.Bytes())

	// write login packet
	loginPacket := struct {
		Reconnecting byte
		Version      uint16
		EncLen       byte
		EncData      []byte
	}{
		Reconnecting: 0,
		Version:      4,
		EncLen:       byte(len(encBlockData)),
		EncData:      encBlockData,
	}
	if err := writePacket(conn, opcode.Id(VERSION, opcode.CL_LOGIN), loginPacket); err != nil {
		return fmt.Errorf("writing login packet: %v", err)
	}

	// read login response
	var loginResponse byte
	if err := binary.Read(reader, binary.BigEndian, &loginResponse); err != nil {
		return fmt.Errorf("reading login response: %v", err)
	}
	if loginResponse != 0 {
		return fmt.Errorf("unknown login response: %d", loginResponse)
	}

	return nil
}

func handlePacket(id byte, data []byte) {
	reader := bytes.NewReader(data)

	op := opcode.Get(VERSION, id)

	switch op {

	case opcode.SV_WELCOME:
		if err := binary.Read(reader, binary.BigEndian, &Info); err != nil {
			log.WithError(err).Error("reading welcome info")
			return
		}
		log.WithField("info", Info).Info("welcome info")

	case opcode.SV_WORLD_INFO:
		if err := binary.Read(reader, binary.BigEndian, &World); err != nil {
			log.WithError(err).Error("reading world data")
			return
		}
		LocalPlayer.Index = World.PlayerIndex
		log.WithField("world", World).Info("world info")

	case opcode.SV_INVENTORY_ITEMS:
		if err := binary.Read(reader, binary.BigEndian, &Inventory.Count); err != nil {
			log.WithError(err).Error("reading inventory item count")
			return
		}
		for i := uint8(0); i < Inventory.Count; i++ {
			if err := binary.Read(reader, binary.BigEndian, &Inventory.Id[i]); err != nil {
				log.WithError(err).WithField("index", i).Error("reading inventory item id")
				continue
			}
			Inventory.Equipped[i] = Inventory.Id[i] / 32768
			Inventory.Id[i] &= 0x7fff
			if itemIntData.Stackable[Inventory.Id[i]] == 0 {
				if err := binary.Read(reader, binary.BigEndian, &Inventory.StackCount[i]); err != nil {
					log.WithError(err).WithField("index", i).Error("reading inventory item stack count")
					continue
				}
			} else {
				Inventory.StackCount[i] = 1
			}
		}
		log.WithField("inventory", Inventory).Info("inventory items")

	case opcode.SV_PLAYER_STAT_EQUIPMENT_BONUS:
		if err := binary.Read(reader, binary.BigEndian, &EquipmentStats); err != nil {
			log.WithError(err).Error("reading player equipment stats")
			return
		}
		log.WithField("stats", EquipmentStats).Info("player equipment stats")

	case opcode.SV_PLAYER_STAT_LIST:
		if err := binary.Read(reader, binary.BigEndian, &Stats); err != nil {
			log.WithError(err).Error("reading player stats")
			return
		}
		log.WithField("stats", Stats).Info("player stats")

	case opcode.SV_PRIVACY_SETTINGS:
		if err := binary.Read(reader, binary.BigEndian, &Settings.Privacy); err != nil {
			log.WithError(err).Error("reading privacy settings")
			return
		}
		log.WithField("settings", Settings.Privacy).Info("privacy settings")

	case opcode.SV_GAME_SETTINGS:
		if err := binary.Read(reader, binary.BigEndian, &Settings.Game); err != nil {
			log.WithError(err).Error("reading game settings")
			return
		}
		log.WithField("settings", Settings.Game).Info("game settings")

	case opcode.SV_FRIEND_LIST:
		if err := binary.Read(reader, binary.BigEndian, &Friends.Count); err != nil {
			log.WithError(err).Error("reading friend count")
			return
		}
		for i := uint8(0); i < Friends.Count; i++ {
			if err := binary.Read(reader, binary.BigEndian, Friends.Hash[i]); err != nil {
				log.WithError(err).WithField("index", i).WithField("count", Friends.Count).Error("reading friend hash")
				return
			}
			if err := binary.Read(reader, binary.BigEndian, Friends.Online[i]); err != nil {
				log.WithError(err).WithField("index", i).WithField("count", Friends.Count).Error("reading friend online")
				return
			}
		}
		log.WithField("friends", Friends).Info("friends list")

	case opcode.SV_IGNORE_LIST:
		if err := binstruct.Read(bufio.NewReader(reader), binary.BigEndian, &Ignore); err != nil {
			log.WithError(err).Error("reading ignore list")
			return
		}
		log.WithField("ignore", Ignore).Info("ignore list")

	case opcode.SV_REGION_PLAYERS:
		var err error
		br := NewBitReader(data)
		if LocalPlayer.X, err = br.Read(11); err != nil {
			log.WithError(err).Error("reading player x")
			return
		}
		if LocalPlayer.Y, err = br.Read(13); err != nil {
			log.WithError(err).Error("reading player y")
			return
		}
		if LocalPlayer.AnimationNext, err = br.Read(4); err != nil {
			log.WithError(err).Error("reading player animation")
			return
		}
		if Players.Count, err = br.Read(8); err != nil {
			log.WithError(err).Error("reading known players count")
			return
		}

		// TODO: this

		log.WithField("count", Players.Count).Info("known player count")

	case opcode.SV_REGION_NPCS:
		br := NewBitReader(data)
		var err error
		if NPC.Count, err = br.Read(8); err != nil {
			log.WithError(err).Error("reading region npc count")
		}
		for i := 0; i < NPC.Count; i++ {
			ll := log.WithField("index", i)
			serverIndex, err := br.Read(16)
			if err != nil {
				ll.WithError(err).Error("reading region npc server index")
				return
			}

			npc := NPC.List[serverIndex]

			needsUpdate, err := br.Read(1)
			if err != nil {
				ll.WithError(err).Error("reading region npc needs update")
				return
			}
			if needsUpdate == 0 {
				continue
			}

			updateType, err := br.Read(1)
			if err != nil {
				ll.WithError(err).Error("reading region npc update type")
				return
			}
			if updateType == 0 { // has moved
				nextAnimation, err := br.Read(3)
				if err != nil {
					ll.WithError(err).Error("reading region npc update animation")
					return
				}
				if npc == nil {
					br.Read(6)
					continue
				}
				wp := npc.Waypoint.Current
				x := npc.Waypoint.X[wp]
				y := npc.Waypoint.Y[wp]
				if nextAnimation == 2 || nextAnimation == 1 || nextAnimation == 3 {
					x += MAGIC_LOC
				}
				if nextAnimation == 6 || nextAnimation == 5 || nextAnimation == 7 {
					x -= MAGIC_LOC
				}
				if nextAnimation == 4 || nextAnimation == 3 || nextAnimation == 5 {
					y += MAGIC_LOC
				}
				if nextAnimation == 0 || nextAnimation == 1 || nextAnimation == 7 {
					y -= MAGIC_LOC
				}
				npc.AnimationNext = nextAnimation
				wp = (wp + 1) % 10
				npc.Waypoint.Current = wp
				npc.Waypoint.X[wp] = x
				npc.Waypoint.Y[wp] = y
			} else { // new sprite/animation
				animationNext, err := br.Read(4)
				if err != nil {
					ll.WithError(err).Error("reading region npc update animation next")
					return
				}
				if (animationNext & 12) == 12 {
					continue
				}
				npc.AnimationNext = animationNext
			}
		}
		log.WithField("npcs", NPC).Info("region npcs")

	case opcode.SV_REGION_OBJECTS:
		count := 0
		for reader.Len() > 0 {
			d, err := reader.ReadByte()
			if err != nil {
				log.WithError(err).Error("reading region objects")
				return
			}
			if d == 255 {
				reader.Seek(4, io.SeekCurrent)
				// TODO: this removes an object, right?
				log.Fatal("SV_REGION_OBJECTS 255 FIXME")
			}
			if _, err := reader.Seek(-1, io.SeekCurrent); err != nil {
				log.WithError(err).Error("seeking start of region object")
				return
			}
			var objData [5]byte
			if err := binary.Read(reader, binary.BigEndian, &objData); err != nil {
				log.WithField("count", count).WithError(err).Error("reading region object")
				return
			}
			obj := Object{
				Id:        binary.BigEndian.Uint16(objData[0:2]),
				X:         LocalPlayer.X + int(objData[2]),
				Y:         LocalPlayer.Y + int(objData[3]),
				Direction: objData[4],
			}
			if obj.Id == 60000 {
				continue
			}
			Objects.List[count] = obj
			count++
		}
		Objects.Count = count

	case opcode.SV_REGION_WALL_OBJECTS:
		count := 0
		for reader.Len() > 0 {
			d, err := reader.ReadByte()
			if err != nil {
				log.WithError(err).Error("reading region wall objects")
				return
			}
			if d == 255 {
				reader.Seek(4, io.SeekCurrent)
				// TODO: this removes an object, right?
				log.Fatal("SV_REGION_WALL_OBJECTS 255 FIXME")
			}
			if _, err := reader.Seek(-1, io.SeekCurrent); err != nil {
				log.WithError(err).Error("seeking start of region wall object")
				return
			}
			var objData [5]byte
			if err := binary.Read(reader, binary.BigEndian, &objData); err != nil {
				log.WithField("count", count).WithError(err).Error("reading region wall object")
				return
			}
			obj := Object{
				Id:        binary.BigEndian.Uint16(objData[0:2]),
				X:         LocalPlayer.X + int(objData[2]),
				Y:         LocalPlayer.Y + int(objData[3]),
				Direction: objData[4],
			}
			if obj.Id == 60000 {
				continue
			}
			WallObjects.List[count] = obj
			count++
		}
		WallObjects.Count = count

	case opcode.SV_REGION_PLAYER_UPDATE:
		var updateCount uint16
		if err := binary.Read(reader, binary.BigEndian, &updateCount); err != nil {
			log.WithError(err).Info("reading region player update count")
			return
		}
		for update := uint16(0); update < updateCount; update++ {

		}

	default:
		log.WithField("id", id).WithField("name", op.Name()).WithField("data", hex.EncodeToString(data)).Fatal("unhandled packet")

	}
}
