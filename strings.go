package main

import (
	"regexp"
	"strings"
)

func usernameLoginHash(username string) int64 {
	s := strings.ToLower(strings.TrimSpace(formatAuthString(username, 12)))

	l := int64(0)
	for j := 0; j < len(s); j++ {
		c := s[j]
		l *= 37
		if c >= 'a' && c <= 'z' {
			l += (1 + int64(c)) - 97
		} else if c >= '0' && c <= '9' {
			l += (27 + int64(c)) - 48
		}
	}

	return l
}

func formatAuthString(s string, maxlen int) string {
	re := regexp.MustCompile("[^a-zA-Z0-9]")
	s = re.ReplaceAllString(s, " ")
	if len(s) > maxlen {
		return s[0:maxlen]
	}
	return s + strings.Repeat(" ", maxlen-len(s))
}
