package log

import "fmt"

type LogEntry []string

func (l LogEntry) WithError(err error) LogEntry {
	return append(l, fmt.Sprintf("error=%q", err))
}

func (l LogEntry) WithField(key string, value interface{}) LogEntry {
	return append(l, parseKV(key, value))
}

func (l LogEntry) WithFields(fields ...interface{}) LogEntry {
	return append(l, WithFields(fields)...)
}

func (l LogEntry) Printf(format string, a ...interface{}) {
	logInfo(fmt.Sprintf(format, a...), l)
}

func (l LogEntry) Info(msg string) {
	logInfo(msg, l)
}

func (l LogEntry) Error(msg string) {
	logError(msg, l)
}

func (l LogEntry) Fatal(msg string) {
	logFatal(msg, l)
}
