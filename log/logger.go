package log

import (
	"fmt"
	"os"
	"strings"
	"time"
)

func parseKV(key, value interface{}) string {
	return fmt.Sprintf("%s=\"%v\"", key, value)
}

func log(level, msg string, fields []string) {
	fmt.Printf("%s[%s] msg=%q %s\n",
		level,
		time.Now().Format("2006-01-02 15:04:05 -0700"),
		msg,
		strings.Join(fields, " "))
}

func logInfo(msg string, fields []string) {
	log("INFO", msg, fields)
}

func logError(msg string, fields []string) {
	log("ERROR", msg, fields)
}

func logFatal(msg string, fields []string) {
	log("FATAL", msg, fields)
	os.Exit(1)
}
