package log

import "fmt"

// why does every structured logging library need like 500 non-vendored deps?!?!
// TODO: replace this with something good

func WithError(err error) LogEntry {
	return LogEntry{parseKV("error", err)}
}

func WithField(key string, value interface{}) LogEntry {
	return LogEntry{parseKV(key, value)}
}

func WithFields(fields ...interface{}) LogEntry {
	entry := make(LogEntry, len(fields)/2)
	if len(entry) == 0 {
		return entry
	}
	for i := 0; i < len(fields); i += 2 {
		if i+1 == len(fields) {
			break
		}
		entry = append(entry, parseKV(fields[i], fields[i+1]))
	}
	return entry
}

func Printf(format string, a ...interface{}) {
	logInfo(fmt.Sprintf(format, a...), nil)
}

func Info(message string) {
	logInfo(message, nil)
}

func Error(message string) {
	logError(message, nil)
}

func Fatal(message string) {
	logFatal(message, nil)
}
