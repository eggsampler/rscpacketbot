package rsccrypto

import "testing"

func TestISAAC_Random(t *testing.T) {
	isaactests := []struct {
		name    string
		seed    []uint32
		results []uint32
	}{
		{
			name: "empty seed",
			seed: []uint32{},
			results: []uint32{
				405143795,
				806046349,
				807101986,
				2961886497,
				695195257,
				2572289769,
				3019876533,
				264870948,
				1594302383,
				1378164207,
			},
		},
		{
			name: "known seed",
			seed: []uint32{1},
			results: []uint32{
				1269481493,
				233172240,
				2220845830,
				1379601027,
				2920528658,
				746088356,
				1194944430,
				1137751264,
				1923481287,
				1098538286,
			},
		},
	}

	for _, tt := range isaactests {
		isaac := NewISAAC(tt.seed)
		for i := 0; i < len(tt.results); i++ {
			if r := isaac.Random(); r != tt.results[i] {
				t.Errorf("%s %v (test %d): got %d, expected %d", tt.name, tt.seed, i, r, tt.results[i])
			}
		}
	}
}
