package rsccrypto

type ISAAC struct {
	randcnt    uint32
	randrsl    [256]uint32
	mm         [256]uint32
	aa, bb, cc uint32
}

var (
	mixNums = [8]uint8{11, 2, 8, 16, 10, 4, 8, 9}
)

func NewISAAC(seed []uint32) *ISAAC {
	i := ISAAC{}
	copy(i.randrsl[:], seed)
	i.init()
	return &i
}

func (isaac *ISAAC) Random() uint32 {
	isaac.randcnt--
	if isaac.randcnt == 0 {
		isaac.isaac()
		isaac.randcnt = 255
	}
	return isaac.randrsl[isaac.randcnt]
}

func (isaac *ISAAC) isaac() {
	isaac.cc++
	isaac.bb += isaac.cc
	for i := 0; i < 256; i++ {
		x := isaac.mm[i]
		switch i & 3 {
		case 0:
			isaac.aa ^= isaac.aa << 13
		case 1:
			isaac.aa ^= isaac.aa >> 6
		case 2:
			isaac.aa ^= isaac.aa << 2
		case 3:
			isaac.aa ^= isaac.aa >> 16
		}
		isaac.aa += isaac.mm[(i+128)&0xff]
		y := isaac.mm[(x&0x3fc)>>2] + isaac.aa + isaac.bb
		isaac.mm[i] = y
		isaac.bb = isaac.mm[((y>>8)&0x3fc)>>2] + x
		isaac.randrsl[i] = isaac.bb
	}
}

func mix(a []uint32) {
	for i := 0; i < 8; i++ {
		if i%2 == 0 {
			a[i] ^= a[(i+1)%8] << mixNums[i]
		} else {
			a[i] ^= a[(i+1)%8] >> mixNums[i]
		}
		a[(i+3)%8] += a[i]
		a[(i+1)%8] += a[(i+2)%8]
	}
}

func (isaac *ISAAC) init() {
	a := [8]uint32{0x9e3779b9, 0x9e3779b9, 0x9e3779b9, 0x9e3779b9, 0x9e3779b9, 0x9e3779b9, 0x9e3779b9, 0x9e3779b9}

	for i := 0; i < 4; i++ {
		mix(a[:])
	}

	for i := 0; i < 256; i += 8 {
		for n := 0; n < 8; n++ {
			a[n] += isaac.randrsl[i+n]
		}
		mix(a[:])
		for n := 0; n < 8; n++ {
			isaac.mm[i+n] = a[n]
		}
	}

	// I believe this second loop using the memory is only performed when a is seed provided to the isaac rng
	// however, as we always will be providing a seed, shouldn't matter
	// TODO: check up on this
	for i := 0; i < 256; i += 8 {
		for n := 0; n < 8; n++ {
			a[n] += isaac.mm[i+n]
		}
		mix(a[:])
		for n := 0; n < 8; n++ {
			isaac.mm[i+n] = a[n]
		}
	}

	isaac.isaac()
	isaac.randcnt = 256
}
