package rsccrypto

import (
	"reflect"
	"testing"
)

/*

func TestRsaDecrypt(t *testing.T) {
	testDecrypt := struct {
		input    []byte
		expected []byte
	}{
		input:    []byte("this is some random text ok?"),
		expected: []byte{72, 91, 16, 132, 6, 82, 137, 18, 133, 85, 127, 205, 193, 87, 241, 141, 3, 172, 234, 216, 211, 146, 200, 208, 7, 254, 155, 198, 255, 210, 56, 100, 33, 193, 231, 238, 252, 165, 100, 76, 97, 112, 185, 10, 233, 165, 11, 1, 40, 8, 169, 17, 246, 42, 215, 169, 140, 160, 178, 54, 11, 87, 169, 106},
	}

	got := RsaDecrypt(testDecrypt.input)
	if !reflect.DeepEqual(got, testDecrypt.expected) {
		t.Errorf("got %v, expected %v", got, testDecrypt.expected)
	}
}

func TestRsaEncrypt(t *testing.T) {
	testEncrypt := struct {
		input    []byte
		expected []byte
	}{
		input:    []byte{72, 91, 16, 132, 6, 82, 137, 18, 133, 85, 127, 205, 193, 87, 241, 141, 3, 172, 234, 216, 211, 146, 200, 208, 7, 254, 155, 198, 255, 210, 56, 100, 33, 193, 231, 238, 252, 165, 100, 76, 97, 112, 185, 10, 233, 165, 11, 1, 40, 8, 169, 17, 246, 42, 215, 169, 140, 160, 178, 54, 11, 87, 169, 106},
		expected: []byte("this is some random text ok?"),
	}

	got := RsaEncrypt(testEncrypt.input)
	if !reflect.DeepEqual(got, testEncrypt.expected) {
		t.Errorf("got %v, expected %v", got, testEncrypt.expected)
	}
}

*/

func TestRsaEncryptDecrypt(t *testing.T) {
	input := []byte("blah")
	encrypted := RsaEncrypt(input)
	decrypted := RsaDecrypt(encrypted)

	if !reflect.DeepEqual(input, decrypted) {
		t.Errorf("got %v, expected %v", input, decrypted)
	}
}
