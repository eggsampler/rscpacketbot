package main

import (
	"bufio"
	"bytes"
	"encoding/binary"
	"errors"
	"fmt"
	"io"

	"bitbucket.org/eggsampler/rscpacketbot/binstruct"
)

func newPacketReader(errChan chan<- error, reader *bufio.Reader) <-chan []byte {

	c := make(chan []byte)

	go func() {
		for {
			packet, err := readPacket(reader)
			if err != nil {
				errChan <- fmt.Errorf("reading packet: %v", err)
				return
			}
			c <- packet
		}
	}()

	return c
}

func readPacket(br *bufio.Reader) ([]byte, error) {
	b, err := br.ReadByte()
	if err != nil {
		return nil, err
	}
	plen := int(b)

	if plen == 1 {
		b, err := br.ReadByte()
		if err != nil {
			return nil, err
		}
		return []byte{b}, nil
	}

	if plen >= 160 {
		b, err := br.ReadByte()
		if err != nil {
			return nil, err
		}
		plen = (plen-160)*256 + int(b)
	}

	buff := make([]byte, plen)
	n, err := br.Read(buff)
	if err != nil {
		return nil, err
	}
	if n != plen {
		return nil, fmt.Errorf("invalid packet length: %d != %d", plen, n)
	}
	if n == 0 {
		return nil, errors.New("no packet read")
	}

	return append(buff[1:], buff[0]), nil
}

func writePacket(w io.Writer, command byte, s interface{}) error { // data []byte) error {
	var writer bytes.Buffer
	if err := binstruct.Write(&writer, binary.BigEndian, s); err != nil {
		return err
	}
	data := writer.Bytes()

	if len(data) == 0 {
		_, err := w.Write([]byte{1, command})
		return err
	}

	if len(data) == 1 {
		_, err := w.Write([]byte{2, data[0], command})
		return err
	}

	plen := len(data) + 1
	if plen >= 160 {
		_, err := w.Write(append([]byte{byte(160 + (plen / 256)), byte(plen), command}, data...))
		return err
	}

	_, err := w.Write(append([]byte{byte(plen), data[len(data)-1], command}, data[:len(data)-1]...))
	return err
}
