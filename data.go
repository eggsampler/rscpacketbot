package main

import (
	"bytes"
	"compress/bzip2"
	"encoding/binary"
	"errors"
	"fmt"
	"io/ioutil"
	"strings"

	"bufio"

	"reflect"

	"bitbucket.org/eggsampler/rscpacketbot/binstruct"
	"bitbucket.org/eggsampler/rscpacketbot/log"
)

var (
	itemIntData struct {
		Count     uint16
		Picture   []uint16 `struct_sizefield:"Count"`
		BasePrice []uint32 `struct_sizefield:"Count"`
		Stackable []uint8  `struct_sizefield:"Count"`
		Unused    []uint8  `struct_sizefield:"Count"`
		Wearable  []uint16 `struct_sizefield:"Count"`
		Mask      []uint32 `struct_sizefield:"Count"`
		Special   []uint8  `struct_sizefield:"Count"`
		Members   []uint8  `struct_sizefield:"Count"`
	}
	itemStrData struct {
		Count       uint16   `struct_skip:"true"`
		Name        []string `struct_sizefield:"Count" struct_delim:"\x00" struct_delimtrim:"true"`
		Description []string `struct_sizefield:"Count" struct_delim:"\x00" struct_delimtrim:"true"`
		Command     []string `struct_sizefield:"Count" struct_delim:"\x00" struct_delimtrim:"true"`
	}

	npcIntData struct {
		Count           uint16
		Attack          []uint8     `struct_sizefield:"Count"`
		Strength        []uint8     `struct_sizefield:"Count"`
		Hits            []uint8     `struct_sizefield:"Count"`
		Defense         []uint8     `struct_sizefield:"Count"`
		Attackable      []uint8     `struct_sizefield:"Count"`
		Sprite          [][12]uint8 `struct_sizefield:"Count"`
		ColourHair      []uint32    `struct_sizefield:"Count"`
		ColourTop       []uint32    `struct_sizefield:"Count"`
		ColourBottom    []uint32    `struct_sizefield:"Count"`
		ColourSkin      []uint32    `struct_sizefield:"Count"`
		Width           []uint16    `struct_sizefield:"Count"`
		Height          []uint16    `struct_sizefield:"Count"`
		WalkModel       []uint8     `struct_sizefield:"Count"`
		CombatModel     []uint8     `struct_sizefield:"Count"`
		CombatAnimation []uint8     `struct_sizefield:"Count"`
	}
	npcStringData struct {
		Count       uint16   `struct_skip:"true"`
		Name        []string `struct_sizefield:"Count" struct_delim:"\x00" struct_delimtrim:"true"`
		Description []string `struct_sizefield:"Count" struct_delim:"\x00" struct_delimtrim:"true"`
		Command     []string `struct_sizefield:"Count" struct_delim:"\x00" struct_delimtrim:"true"`
	}

	textureIntData struct {
		Count uint16
	}
	textureStrData struct {
		Count uint16   `struct_skip:"true"`
		Name  []string `struct_sizefield:"Count" struct_delim:"\x00" struct_delimtrim:"true"`
		Type  []string `struct_sizefield:"Count" struct_delim:"\x00" struct_delimtrim:"true"`
	}

	animationIntData struct {
		Count           uint16
		CharacterColour []uint32 `struct_sizefield:"Count"`
		HeadType        []uint8  `struct_sizefield:"Count"`
		A               []uint8  `struct_sizefield:"Count"`
		F               []uint8  `struct_sizefield:"Count"`
		Number          []uint8  `struct_sizefield:"Count"`
	}
	animationStrData struct {
		Count uint16   `struct_skip:"true"`
		Name  []string `struct_sizefield:"Count" struct_delim:"\x00" struct_delimtrim:"true"`
	}

	objectIntData struct {
		Count     uint16
		Width     []uint8 `struct_sizefield:"Count"`
		Height    []uint8 `struct_sizefield:"Count"`
		Type      []uint8 `struct_sizefield:"Count"`
		Elevation []uint8 `struct_sizefield:"Count"`
	}
	objectStrData struct {
		Count       uint16   `struct_skip:"true"`
		Name        []string `struct_sizefield:"Count" struct_delim:"\x00" struct_delimtrim:"true"`
		Description []string `struct_sizefield:"Count" struct_delim:"\x00" struct_delimtrim:"true"`
		Command1    []string `struct_sizefield:"Count" struct_delim:"\x00" struct_delimtrim:"true"`
		Command2    []string `struct_sizefield:"Count" struct_delim:"\x00" struct_delimtrim:"true"`
		ModelName   []string `struct_sizefield:"Count" struct_delim:"\x00" struct_delimtrim:"true"`
	}

	wallObjectIntData struct {
		Count        uint16
		Height       []uint16 `struct_sizefield:"Count"`
		TextureFront []uint32 `struct_sizefield:"Count"`
		TextureBack  []uint32 `struct_sizefield:"Count"`
		Adjacent     []uint8  `struct_sizefield:"Count"`
		Invisible    []uint8  `struct_sizefield:"Count"`
	}
	wallObjectStrData struct {
		Count       uint16   `struct_skip:"true"`
		Name        []string `struct_sizefield:"Count" struct_delim:"\x00" struct_delimtrim:"true"`
		Description []string `struct_sizefield:"Count" struct_delim:"\x00" struct_delimtrim:"true"`
		Command1    []string `struct_sizefield:"Count" struct_delim:"\x00" struct_delimtrim:"true"`
		Command2    []string `struct_sizefield:"Count" struct_delim:"\x00" struct_delimtrim:"true"`
	}

	roofIntData struct {
		Count       uint16
		Height      []uint8 `struct_sizefield:"Count"`
		NumVertices []uint8 `struct_sizefield:"Count"`
	}

	tileIntData struct {
		Count      uint16
		Decoration []uint32 `struct_sizefield:"Count"`
		Type       []uint8  `struct_sizefield:"Count"`
		Adjacent   []uint8  `struct_sizefield:"Count"`
	}

	projectileIntData struct {
		Sprite uint16
	}

	spellIntData struct {
		Count         uint16
		Level         []uint8 `struct_sizefield:"Count"`
		RunesRequired []uint8 `struct_sizefield:"Count"`
		Type          []uint8 `struct_sizefield:"Count"`
		RuneIds       []struct {
			Count uint8
			Id    []uint16 `struct_sizefield:"Count"`
		} `struct_sizefield:"Count"`
		RuneAmounts []struct {
			Count  uint8
			Amount []uint8 `struct_sizefield:"Count"`
		} `struct_sizefield:"Count"`
	}
	spellStrData struct {
		Count       uint16   `struct_skip:"true"`
		Name        []string `struct_sizefield:"Count" struct_delim:"\x00" struct_delimtrim:"true"`
		Description []string `struct_sizefield:"Count" struct_delim:"\x00" struct_delimtrim:"true"`
	}

	prayerIntData struct {
		Count uint16
		Level []uint8 `struct_sizefield:"Count"`
		Drain []uint8 `struct_sizefield:"Count"`
	}
	prayerStrData struct {
		Count       uint16   `struct_skip:"true"`
		Name        []string `struct_sizefield:"Count" struct_delim:"\x00" struct_delimtrim:"true"`
		Description []string `struct_sizefield:"Count" struct_delim:"\x00" struct_delimtrim:"true"`
	}
)

func loadData() error {
	buff, err := readDataFile("data/config85.jag")
	if err != nil {
		return err
	}

	strData, err := unpackSubFile("string.dat", buff)
	if err != nil {
		return err
	}
	// ioutil.WriteFile("string.dat", strData, 0777)

	intData, err := unpackSubFile("integer.dat", buff)
	if err != nil {
		return err
	}
	// ioutil.WriteFile("integer.dat", intData, 0777)

	intReader := bufio.NewReader(bytes.NewReader(intData))
	strReader := bufio.NewReader(bytes.NewReader(strData))

	datas := []struct {
		Name    string
		IntData interface{}
		StrData interface{}
	}{
		{"item", &itemIntData, &itemStrData},
		{"npc", &npcIntData, &npcStringData},
		{"texture", &textureIntData, &textureStrData},
		{"animation", &animationIntData, &animationStrData},
		{"object", &objectIntData, &objectStrData},
		{"wallObject", &wallObjectIntData, &wallObjectStrData},
		{"roof", &roofIntData, nil},
		{"tile", &tileIntData, nil},
		{"projectile", &projectileIntData, nil},
		{"spell", &spellIntData, &spellStrData},
		{"prayer", &prayerIntData, &prayerStrData},
	}

	for _, d := range datas {
		if err := loadDataStructs(d.Name, intReader, strReader, d.IntData, d.StrData); err != nil {
			return err
		}
	}

	return nil
}

func loadDataStructs(name string, intReader, strReader *bufio.Reader, intData, strData interface{}) error {
	// read int data
	if err := binstruct.Read(intReader, binary.BigEndian, intData); err != nil {
		return fmt.Errorf("reading %s int data: %v", name, err)
	}
	if strData == nil {
		return nil
	}
	// copy count
	count := reflect.ValueOf(intData).Elem().FieldByName("Count").Uint()
	reflect.ValueOf(strData).Elem().FieldByName("Count").SetUint(count)
	// read str data
	if err := binstruct.Read(strReader, binary.BigEndian, strData); err != nil {
		return fmt.Errorf("reading %s str data: %v", name, err)
	}
	log.WithField("name", name).WithField("count", count).Info("loaded data")
	return nil
}

func readDataFile(path string) ([]byte, error) {
	buff, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, fmt.Errorf("reading file %v", err)
	}
	if len(buff) < 6 {
		return nil, errors.New("invalid file length, must be > 6")
	}

	// why no uint24 !!!
	decompressedSize := binary.BigEndian.Uint32(append([]byte{0}, buff[0:3]...))
	compressedSize := binary.BigEndian.Uint32(append([]byte{0}, buff[3:6]...))
	buff = buff[6:]

	if compressedSize != uint32(len(buff)) {
		return nil, fmt.Errorf("invalid file size - expected: %d, got: %d", compressedSize, len(buff))
	}

	if decompressedSize != compressedSize {
		buff, err = decompress(buff, decompressedSize)
		if err != nil {
			return nil, err
		}
	}

	return buff, nil
}

func decompress(buff []byte, decompressedSize uint32) ([]byte, error) {
	// add a few headers to the buffer to trick go's bzip2 reader into actually reading it
	r := bzip2.NewReader(bytes.NewReader(append([]byte{'B', 'Z', 'h', '1'}, buff...)))
	dbuff := make([]byte, decompressedSize)
	offset := uint32(0)

	for offset < decompressedSize {
		// possibly multiple compressed sub-sections inside
		n, err := r.Read(dbuff[offset:])
		if err != nil {
			return nil, fmt.Errorf("decompressing file at offset %d: %v", offset, err)
		}
		if n <= 0 {
			return nil, fmt.Errorf("invalid decompressed size at offset %d: %d", offset, n)
		}
		offset += uint32(n)
	}

	if decompressedSize != offset {
		return nil, fmt.Errorf("invalid decompressed file size - expected: %d, got %d", decompressedSize, offset)
	}

	return dbuff, nil
}

func unpackSubFile(fileName string, buff []byte) ([]byte, error) {
	entries := binary.BigEndian.Uint16(buff)
	offset := 2 + uint32(entries)*10

	fileName = strings.ToUpper(fileName)
	var fileHash int32
	for i := 0; i < len(fileName); i++ {
		fileHash = (fileHash*61 + int32(fileName[i])) - 32
	}

	// log.Printf("File %s - hash %d\n", fileName, fileHash)

	for entry := uint16(0); entry < entries; entry++ {
		entryIndex := entry*10 + 2
		entryHash := int32(binary.BigEndian.Uint32(buff[entryIndex : entryIndex+4]))
		decompressedSize := binary.BigEndian.Uint32(append([]byte{0}, buff[entryIndex+4:entryIndex+7]...))
		compressedSize := binary.BigEndian.Uint32(append([]byte{0}, buff[entryIndex+7:entryIndex+10]...))

		// log.Printf(" %d - decompressed %d, compressed %d\n", entryHash, decompressedSize, compressedSize)

		if entryHash == fileHash {
			if decompressedSize != compressedSize {
				return decompress(buff[offset:], compressedSize)
			}
			return buff[offset : offset+decompressedSize], nil
		}

		offset += compressedSize
	}

	return nil, errors.New("sub file not found")
}
