package main

import (
	"encoding/binary"
	"testing"
)

func TestBitReader_Read(t *testing.T) {
	tests := []int{123456}
	for _, testInt := range tests {
		b := make([]byte, 4)
		binary.BigEndian.PutUint32(b, uint32(testInt))
		br := NewBitReader(b)
		readInt, err := br.Read(32)
		if err != nil {
			t.Fatal(err)
		}
		if testInt != readInt {
			t.Fatalf("expected %d, got %d", testInt, readInt)
		}
	}
}
