package binstruct

import (
	"errors"
	"fmt"
	"reflect"
	"strconv"
)

// TODO: work on this a bit more as needed, make it a separate library and put it in the vendor folder
// TODO: write some tests

var (
	ErrNoData    = errors.New("no data provided")
	ErrNotStruct = errors.New("data is not pointer to struct")
)

type structFieldOptions struct {
	skip         bool
	sizeFixed    int
	sizeField    string
	delimPresent bool
	delimByte    byte
	delimTrim    bool
	writeSize    uint64
}

type StructFieldContext struct {
	ParentStruct reflect.Value
	Index        int
}

func parseTags(tag reflect.StructTag) (structFieldOptions, error) {
	options := structFieldOptions{}

	if v, ok := tag.Lookup("struct_skip"); ok && v != "" {
		options.skip = true
	}

	if v, ok := tag.Lookup("struct_sizefixed"); ok && v != "" {
		n, err := strconv.ParseUint(v, 10, 64)
		if err != nil {
			return options, fmt.Errorf("parsing tag struct_sizefixed (%s): %v", v, err)
		}
		s := int(n)
		if s < 0 {
			return options, fmt.Errorf("negative size: %d", s)
		}
		options.sizeFixed = s
	}

	if v, ok := tag.Lookup("struct_sizefield"); ok && v != "" {
		options.sizeField = v
	}

	if v, ok := tag.Lookup("struct_delim"); ok && v != "" {
		options.delimByte = v[0]
		options.delimPresent = true
	}

	if v, ok := tag.Lookup("struct_delimtrim"); ok && v != "" {
		options.delimTrim = true
	}

	if v, ok := tag.Lookup("struct_writesize"); ok && v != "" {
		var err error
		options.writeSize, err = strconv.ParseUint(v, 10, 64)
		if err != nil {
			return options, fmt.Errorf("parsing tag struct_writesize (%d): %v", options.writeSize, err)
		}
	}

	return options, nil
}
