package binstruct

import (
	"encoding/binary"
	"errors"
	"fmt"
	"io"
	"reflect"
)

type structWriter struct {
	order  binary.ByteOrder
	writer io.Writer
}

func Write(w io.Writer, order binary.ByteOrder, data interface{}) error {
	if data == nil {
		return ErrNoData
	}

	d := structWriter{
		order:  order,
		writer: w,
	}

	err := d.write(reflect.ValueOf(data), structFieldOptions{})
	if err != nil {
		return err
	}

	return nil
}

func (d *structWriter) write(v reflect.Value, fieldOpts structFieldOptions) error {
	if fieldOpts.skip {
		return nil
	}

	switch v.Kind() {

	case reflect.Struct:
		for i := 0; i < v.NumField(); i++ {
			// iterate through each field on the struct
			structField := v.Field(i)
			// if the field can't be read, skip it
			if !structField.CanInterface() {
				continue
			}
			// get the info about the field
			typeField := v.Type().Field(i)
			// parse any options in the field tags
			typeFieldOptions, err := parseTags(typeField.Tag)
			if err != nil {
				return fmt.Errorf("parsing tags in field %s: %v", typeField.Name, err)
			}
			// write the field
			if err := d.write(structField, typeFieldOptions); err != nil {
				return fmt.Errorf("writing stuct field %s (%d): %v", typeField.Name, i, err)
			}
		}

	case reflect.Array, reflect.Slice:
		if fieldOpts.writeSize > 0 {
			// TODO: more than just a byte
			if err := d.writeByte(byte(v.Len())); err != nil {
				return fmt.Errorf("writing size: %v", err)
			}
		}
		// loop through each element and write
		for i := 0; i < v.Len(); i++ {
			if err := d.write(v.Index(i), fieldOpts); err != nil {
				return fmt.Errorf("writing %s: %v", v.Kind().String(), err)
			}
		}

	case reflect.String:
		if fieldOpts.writeSize > 0 {
			// TODO: more than just a byte
			if err := d.writeByte(byte(v.Len())); err != nil {
				return fmt.Errorf("writing size: %v", err)
			}
		}
		// write the string
		n, err := d.writer.Write([]byte(v.String()))
		if err != nil {
			return fmt.Errorf("writing string: %v", err)
		}
		// check that the size was actually written
		if n != v.Len() {
			return errors.New("didn't write full string")
		}
		// write a delimiter if present
		if fieldOpts.delimPresent {
			if err := d.writeByte(fieldOpts.delimByte); err != nil {
				return fmt.Errorf("writing string delim: %v", err)
			}
		}

	default:
		return binary.Write(d.writer, d.order, v.Interface())

	}

	return nil
}

func (d *structWriter) writeByte(b byte) error {
	n, err := d.writer.Write([]byte{b})
	if err != nil {
		return err
	}
	if n != 1 {
		return fmt.Errorf("wrong size written")
	}
	return nil
}
