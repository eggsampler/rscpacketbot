package binstruct

import (
	"bufio"
	"encoding/binary"
	"errors"
	"fmt"
	"reflect"
)

type structReader struct {
	order  binary.ByteOrder
	reader *bufio.Reader
	data   interface{}
}

func Read(r *bufio.Reader, order binary.ByteOrder, data interface{}) error {
	if data == nil {
		return ErrNoData
	}

	v := reflect.ValueOf(data)
	if v.Kind() != reflect.Ptr {
		return ErrNotStruct
	}

	d := structReader{
		order:  order,
		reader: r,
	}

	return d.read(v.Elem(), StructFieldContext{Index: -1}, structFieldOptions{})
}

func (d *structReader) read(v reflect.Value, ctx StructFieldContext, fieldOpts structFieldOptions) error {
	if fieldOpts.skip {
		return nil
	}

	switch v.Kind() {

	case reflect.Struct:
		for i := 0; i < v.NumField(); i++ {
			// iterate through each field on the struct
			structField := v.Field(i)
			// if the field can't be set (ie, not exported), skip it
			if !structField.CanAddr() {
				continue
			}
			// get the info about the field
			typeField := v.Type().Field(i)
			// parse any options in the field tags
			typeFieldOptions, err := parseTags(typeField.Tag)
			if err != nil {
				return fmt.Errorf("parsing tags in field %s: %v", typeField.Name, err)
			}
			// set the parent as the current value
			fieldCtx := StructFieldContext{
				ParentStruct: v,
				Index:        -1,
			}
			// read the field
			if err := d.read(structField, fieldCtx, typeFieldOptions); err != nil {
				return fmt.Errorf("reading struct field %s (%d): %v", typeField.Name, i, err)
			}
		}

	case reflect.Slice:
		// get the size of the slice to read from the options
		size, err := getSize(ctx, fieldOpts)
		if err != nil {
			return fmt.Errorf("getting slice size: %v", err)
		}
		// if theres no size, skip
		if size == 0 {
			return nil
		}
		// make a slice of the given size (and size as capacity) and set it
		v.Set(reflect.MakeSlice(v.Type(), size, size))
		// loop through each element and read
		for i := 0; i < size; i++ {
			ctx.Index = i
			if err := d.read(v.Index(i), ctx, fieldOpts); err != nil {
				return fmt.Errorf("reading slice index %d/%d - %+v: %v", i, size, fieldOpts, err)
			}
		}

	case reflect.Array:
		// loop through each element and read
		for i := 0; i < v.Len(); i++ {
			ctx.Index = i
			if err := d.read(v.Index(i), ctx, fieldOpts); err != nil {
				return fmt.Errorf("reading array index %d/%d: %v", i, v.Len(), err)
			}
		}

	case reflect.String:
		// if a delimiter is set
		if fieldOpts.delimPresent {
			// use the buffered reader to read to the delimiter
			s, err := d.reader.ReadString(fieldOpts.delimByte)
			if err != nil {
				return fmt.Errorf("reading string delim: %v", err)
			}
			// if trim the delimiter, just remove the last byte
			if fieldOpts.delimTrim {
				s = s[:len(s)-1]
			}
			// set the value
			v.SetString(s)
			return nil
		}
		// get the string size from the options
		size, err := getSize(ctx, fieldOpts)
		if err != nil {
			return fmt.Errorf("getting string size: %v", err)
		}
		// read the size from the reader
		b := make([]byte, size)
		n, err := d.reader.Read(b)
		if err != nil {
			return fmt.Errorf("reading string: %v", err)
		}
		// check that the set size is actually read
		if size != n {
			return fmt.Errorf("didn't read string fully: expected %d, got %d", size, n)
		}
		// and set the value
		v.SetString(string(b))

	default:
		ptr := v.Addr().Interface()
		return binary.Read(d.reader, d.order, ptr)

	}

	return nil
}

func getSize(ctx StructFieldContext, options structFieldOptions) (int, error) {
	// if there's a fixed size for the field, just return that
	// assume that a fixed size will always be greater than 0
	if options.sizeFixed > 0 {
		return options.sizeFixed, nil
	}

	// if a size field is set
	if options.sizeField != "" {
		// check the parent struct is valid
		// TODO: is this necessary?
		if !ctx.ParentStruct.IsValid() {
			return 0, errors.New("no parent struct to fetch size from")
		}
		// get the field by name
		sizeField := ctx.ParentStruct.FieldByName(options.sizeField)
		// and check it's actually a real field
		if !sizeField.IsValid() {
			return 0, fmt.Errorf("invalid field name: %s", options.sizeField)
		}

		// read the size field
		switch sizeField.Kind() {

		case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
			// signed integer value
			s := int(sizeField.Int())
			// check for negative value / overflow
			if s < 0 {
				return 0, fmt.Errorf("negative sizefield %s: %d", options.sizeField, s)
			}
			return s, nil

		case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64, reflect.Uintptr:
			// unsigned integer value
			s := int(sizeField.Uint())
			// check for overflow
			if s < 0 {
				return 0, fmt.Errorf("negative sizefield %s: %d", options.sizeField, s)
			}
			return s, nil

		default:
			// only support signed/unsigned integers for type fields
			return 0, fmt.Errorf("unsupported sizefield %s kind: %s", options.sizeField, sizeField.Kind())
		}
	}

	// if theres no fixed size or size field present
	return 0, errors.New("no fixed size or size field present")
}
